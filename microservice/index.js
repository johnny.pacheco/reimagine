
const express = require('express');
const multer = require('multer');
const sharp = require('sharp');
const storage = require('./upload-config');
const upload = multer(storage);
const path = require('path');
const fs = require('fs');
const app = express();

const router = new express.Router;
app.use(router);

router.get('/', (req, res) => {
    res.send('ok')
});

app.use(express.static('uploads'));

router.post('/upload',upload.single('image'), async (req, res) => {
   const { filename: image } = req.file;

    //Guardamos grande
   await sharp(req.file.path)
    .resize(500)
    .jpeg({quality: 60})
    .toFile(
        path.resolve(req.file.destination,'resized',image)
    );

    //Guardamos miniatura
    await sharp(req.file.path)
    .resize(300)
    .jpeg({quality: 30})
    .toFile(
        path.resolve(req.file.destination,'thumbnail',image)
    );
    fs.unlinkSync(req.file.path);

    return res.send('SUCCESS!');
})
app.listen(3333, () => {
    console.log('server on!');
})